<h3>Latihan OOP</h3>
<?php

require('Animal.php');
require("ape.php");
require('frog.php');

$Hewanpeliharaan = new Animal("Kambing");
echo "Nama Hewan = " . $Hewanpeliharaan->hewan . "<br>";
echo "Jumlah Kaki = " . $Hewanpeliharaan->leg . "<br>";
echo "Cold Blooded = " . $Hewanpeliharaan->blood . "<br><br>";

$SunGoKong = new Ape("Kera Sakti");
echo "Nama Hewan = " . $SunGoKong->hewan . "<br>";
echo "Jumlah Kaki = " . $SunGoKong->leg . "<br>";
echo "Cold Blooded = " . $SunGoKong->blood . "<br>";
echo "Yell = " . $SunGoKong->yell . "<br><br>";

$Kodok = new Frog("Kodok Budug");
echo "Nama Hewan = " . $Kodok->hewan . "<br>";
echo "Jumlah Kaki = " . $Kodok->leg . "<br>";
echo "Cold Blooded = " . $Kodok->blood . "<br>";
$Kodok->jump() . "<br><br>";

?>